var mysql  = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view resume_view as
 select * from resume

 */

exports.getAll = function(callback) {
    var query = 'select g.*, DName, PlName from game g join developer d on g.developer_id = d.developer_id join platform p on g.platform_id = p.platform_id';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getGameReviews = function(callback) {
    var query = 'select * from GetGameReviews;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getGameReviewsByMinAvgScore = function(score, callback) {
    var query = 'select g.*, DName, PlName, avg(score) as avg_score from game_review_score g join developer d on g.developer_id = d.developer_id join platform p on g.platform_id = p.platform_id group by g.GName having avg_score >= ?';

    connection.query(query, score, function(err, result) {
        callback(err, result);
    });
};

exports.getByPlatform = function(platform_id, callback) {
    var query = 'select g.*, DName, PlName from game g join developer d on g.developer_id = d.developer_id join platform p on g.platform_id = p.platform_id where p.platform_id = ?';

    connection.query(query, platform_id, function(err, result) {
        callback(err, result);
    });
};

exports.getDevelopers = function(callback) {
    var query = 'SELECT * FROM developer;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getGames = function(callback) {
    var query = 'SELECT * FROM game;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getPlatforms = function(callback) {
    var query = 'SELECT * FROM platform;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function (params, callback)
{
    var query = 'call InsertGameAndReview (?,?,?,?,?,?,?);';
    var queryParams = [params.GName, params.isBeaten, params.platform_id,
        params.developer_id, params.units_sold_worldwide, params.rev_name, params.score];

    connection.query(query, queryParams, function (err, result)
    {
        callback(err, result);
    });

};

exports.insertGameReview = function (params, callback)
{
    var query = 'INSERT INTO game_review_score (GName, platform_id, developer_id, rev_name, score) VALUES (?,?,?,?,?)';
    var queryParams = [params.GName,params.platform_id, params.developer_id,
        params.rev_name, params.score];

    connection.query(query, queryParams, function (err, result)
    {
        callback(err, result);
    });

};

exports.edit = function (params, callback)
{
    var query = 'select * from game where GName = ? and developer_id = ? and platform_id = ?';
    var queryParams = [params.GName, params.developer_id,
        params.platform_id];

    connection.query(query, queryParams, function (err, result)
    {
        callback(err, result);
    });
};

exports.editGameReview = function (params, callback)
{
    var query = 'select * from game_review_score where GName = ? and developer_id = ? and platform_id = ? and rev_name = ?';
    var queryParams = [params.GName, params.developer_id,
        params.platform_id, params.rev_name];

    connection.query(query, queryParams, function (err, result)
    {
        callback(err, result);
    });
};

exports.update = function (params, callback)
{
    var query = 'update game set developer_id = ?, platform_id = ? where GName = ? and developer_id = ? and platform_id = ?';
    var queryParams = [params.developer_id, params.platform_id,
        params.oldGName, params.old_developer_id, params.old_platform_id];

    connection.query(query, queryParams, function (err, result)
    {
        callback(err, result);
    });

};

exports.updateGameReview = function (params, callback)
{
    var query = 'update game_review_score set rev_name = ?, score = ? where GName = ? and developer_id = ? and platform_id = ? and rev_name = ?';
    var queryParams = [params.rev_name, params.score,
        params.oldGName, params.old_developer_id, params.old_platform_id, params.old_rev_name];

    connection.query(query, queryParams, function (err, result)
    {
        callback(err, result);
    });

};

exports.delete = function (params, callback)
{
    var query = 'DELETE FROM game WHERE GName = ? and developer_id = ? and platform_id = ?';
    var queryParams = [params.GName, params.developer_id,
        params.platform_id];

    connection.query(query, queryParams, function (err, result)
    {
        callback(err, result);
    });

};

exports.deleteGameReview = function (params, callback)
{
    var query = 'delete from game_review_score where GName = ? and developer_id = ? and platform_id = ? and rev_name = ?';
    var queryParams = [params.GName, params.developer_id,
        params.platform_id, params.rev_name];

    connection.query(query, queryParams, function (err, result)
    {
        callback(err, result);
    });

};