var mysql  = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view platform_view as
 select * from resume
 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM platform;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function (params, callback)
{
    var query = 'INSERT INTO platform (PlName) VALUES (?)';
    var queryParams = [params.PlName];

    connection.query(query, queryParams, function (err, result)
    {
        callback(err, result);
    });

};

exports.delete = function (platform_id, callback)
{
    var query = 'DELETE FROM platform WHERE platform_id = ?';

    connection.query(query, platform_id, function (err, result)
    {
        callback(err, result);
    });

};

exports.edit = function (platform_id, callback)
{
    var query = 'SELECT * FROM platform WHERE platform_id = ?';
    var queryData = [platform_id];

    connection.query(query, queryData, function (err, result)
    {
        callback(err, result);
    });
};

exports.update = function (params, callback)
{
    var query = 'UPDATE platform SET PlName = ? WHERE platform_id = ?';
    var queryData = [params.PlName, params.platform_id];

    connection.query(query, queryData, function (err, result)
    {
        callback(err, result);
    });
};