var mysql  = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view developer_view as
 select * from resume
 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM developer;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function (params, callback)
{
    var query = 'INSERT INTO developer (DName) VALUES (?)';
    var queryParams = [params.DName];

    connection.query(query, queryParams, function (err, result)
    {
        callback(err, result);
    });

};

exports.delete = function (developer_id, callback)
{
    var query = 'DELETE FROM developer WHERE developer_id = ?';

    connection.query(query, developer_id, function (err, result)
    {
        callback(err, result);
    });

};

exports.edit = function (developer_id, callback)
{
    var query = 'SELECT * FROM developer WHERE developer_id = ?';
    var queryData = [developer_id];

    connection.query(query, queryData, function (err, result)
    {
        callback(err, result);
    });
};

exports.update = function (params, callback)
{
    var query = 'UPDATE developer SET DName = ? WHERE developer_id = ?';
    var queryData = [params.DName, params.developer_id];

    connection.query(query, queryData, function (err, result)
    {
        callback(err, result);
    });
};