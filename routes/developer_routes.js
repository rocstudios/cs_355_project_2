var express = require('express');
var router = express.Router();
var developer_dal = require('../model/developer_dal');


// View all developers
router.get('/all', function(req, res) {
    developer_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('developer/developerViewAll', { 'result':result });
        }
    });

});

// Add a developer
router.get('/add', function(req, res) {
    res.render('developer/developerAdd');
});

// Insert a developer
router.get('/insert', function (req, res)
{
    // simple validation
    if (req.query.DName == null)
    {
        res.send('Developer Name must be provided.');
    }
    else
    {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        developer_dal.insert(req.query, function (err, result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //res.redirect(302, '/developer/all');
                res.send('success');
            }
        });
    }
});

router.get('/edit', function (req, res)
{
    if (req.query.DName == null)
    {
        res.send('A developer name is required');
    }
    else
    {
        developer_dal.edit(req.query.DName, function (err, result)
        {
            res.render('developer/developerUpdate', {developer: result[0]});
        });
    }

});

router.get('/update', function (req, res)
{
    developer_dal.update(req.query, function (err, result)
    {
        //res.redirect(302, '/developer/all');
        res.send('success');
    });
});

router.get('/delete', function (req, res)
{
    developer_dal.delete(req.query.developer_id, function (err, result)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
            res.redirect(302, '/developer/all');
        }
    });
});

module.exports = router;