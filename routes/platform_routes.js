var express = require('express');
var router = express.Router();
var platform_dal = require('../model/platform_dal');


// View all platforms
router.get('/all', function(req, res) {
    platform_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('platform/platformViewAll', { 'result':result });
        }
    });

});

// Add a platform
router.get('/add', function(req, res) {
    res.render('platform/platformAdd');
});

// Insert a platform
router.get('/insert', function (req, res)
{
    // simple validation
    if (req.query.PlName == null)
    {
        res.send('Platform Name must be provided.');
    }
    else
    {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        platform_dal.insert(req.query, function (err, result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //res.redirect(302, '/platform/all');
                res.send('success');
            }
        });
    }
});

router.get('/edit', function (req, res)
{
    if (req.query.PlName == null)
    {
        res.send('A platform name is required');
    }
    else
    {
        platform_dal.edit(req.query.PlName, function (err, result)
        {
            res.render('platform/platformUpdate', {platform: result[0]});
        });
    }

});

router.get('/update', function (req, res)
{
    platform_dal.update(req.query, function (err, result)
    {
        //res.redirect(302, '/platform/all');
        res.send('success');
    });
});

router.get('/delete', function (req, res)
{
    platform_dal.delete(req.query.platform_id, function (err, result)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
            res.redirect(302, '/platform/all');
        }
    });
});

module.exports = router;