var express = require('express');
var router = express.Router();
var game_dal = require('../model/game_dal');


// View All games
router.get('/all', function(req, res) {
    game_dal.getAll(function(err, result){
        game_dal.getPlatforms(function (err, result2){
            game_dal.getGameReviews(function (err, result3) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('game/gameViewAll', {'result': result, 'result2': result2, 'result3': result3});
                }
            });
        });
    });

});

// View games by a particular platform
router.get('/gamesByPlatform', function(req, res) {
    game_dal.getByPlatform(req.query.platform_id, function(err, result){
        game_dal.getPlatforms(function (err, result2) {
            game_dal.getGameReviews(function (err, result3) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('game/gameViewByPlatform', {'result': result, 'result2': result2, 'result3': result3});
                }
            });
        });
    });

});

// View game reviews by a selected min average score
router.get('/gamesByReviewScore', function(req, res) {
    game_dal.getAll(function(err, result){
        game_dal.getPlatforms(function (err, result2){
            game_dal.getGameReviewsByMinAvgScore(req.query.score, function (err, result3) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('game/gameViewByReviewScore', {'result': result, 'result2': result2, 'result3': result3});
                }
            });
        });
    });
});

// Return the add a new game form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    game_dal.getDevelopers(function(err,result1) {
        game_dal.getPlatforms(function(err,result2) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('game/gameAdd', {'result1': result1,
                    'result2': result2});
            }
        });
    });
});

// Return the add a new game form
router.get('/addGameReview', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    game_dal.getGames(function(err,result) {
        game_dal.getDevelopers(function(err,result1) {
            game_dal.getPlatforms(function(err,result2) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('game/gameAddReview', {'result': result, 'result1': result1, 'result2': result2
                    });
                }
            });
        });
    });
});

router.get('/insert', function (req, res)
{
    // simple validation
    if (req.query.GName == "")
    {
        res.send('Game name must be provided.');
    }
    if (req.query.rev_name == "")
    {
        res.send('Reviewer name must be provided.');
    }
    if (req.query.score == "")
    {
        res.send('Score must be provided.');
    }
    else
    {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        game_dal.insert(req.query, function (err, result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //res.redirect(302, '/game/all');
                res.send('success');
            }
        });
    }
});

router.get('/insertGameReview', function (req, res)
{
    // simple validation
    if (req.query.rev_name == "")
    {
        res.send('Review name must be provided.');
    }
    if (req.query.score == "")
    {
        res.send('Game score must be provided.');
    }
    else
    {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        game_dal.insertGameReview(req.query, function (err, result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //res.redirect(302, '/game/all');
                res.send('success');
            }
        });
    }
});

router.get('/edit', function (req, res)
{
    game_dal.getDevelopers(function (err, result2) {
        game_dal.getPlatforms(function (err, result3) {
            game_dal.edit(req.query, function (err, result) {
                res.render('game/gameUpdate', {result: result[0], result2: result2, result3: result3});
            });
        });
    });
});

router.get('/editGameReview', function (req, res)
{
    game_dal.editGameReview(req.query, function (err, result) {
        res.render('game/gameReviewScoreUpdate', {result: result[0]});
    });
});

router.get('/update', function (req, res)
{
    game_dal.update(req.query, function (err, result)
    {
        //res.redirect(302, '/game/all');
        res.send('success');
    });
});

router.get('/updateGameReview', function (req, res)
{
    game_dal.updateGameReview(req.query, function (err, result)
    {
        //res.redirect(302, '/game/all');
        res.send('success');
    });
});

router.get('/delete', function (req, res)
{
    game_dal.delete(req.query, function (err, result)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
            res.redirect(302, '/game/all');
        }
    });
});

router.get('/deleteGameReview', function (req, res)
{
    game_dal.deleteGameReview(req.query, function (err, result)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
            res.redirect(302, '/game/all');
        }
    });
});

router.get('/about', function(req, res) {
    res.render('game/about');
});

module.exports = router;
